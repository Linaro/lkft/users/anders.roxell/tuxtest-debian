# Tuxtest debian

This repository contains configurations to build debian root file systems for
running in LAVA with the TuxText backend. The tool that are used to generate the debian root file systems are mmdebstrap.

## Building

You can now build supported architecture one by one:

```shell
make ARCH=amd64
make ARCH=arm64
make ARCH=armhf
make ARCH=i386
make ARCH=mipsel
make ARCH=mips64el
make ARCH=ppc64el
make ARCH=riscv64
make ARCH=s390x
```

The releases that can be passed in to make are bullseye, bookworm and sid.

```shell
make ARCH=arm64 RELEASE=bookworm
```

## Uploading

Every root filesystems and kernels are uploaded to https://storage.tuxboot.com/debian/<release>

To upload the `arm64` debian bookworm root file system:

```shell
make upload ARCH=arm64 RELEASE=bookworm
```
