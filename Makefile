# Parameters
TUXTESTS ?= ltp-smoke
ENV ?= prod
RELEASE ?= bookworm

# Constants
ARCHS = amd64 arm64 armhf i386 mipsel mips64el ppc64el riscv64 s390x
ARTEFACTS = rootfs.ext4.xz rootfs.tar.xz
UPLOADS = $(foreach artefact,$(ARTEFACTS),upload-$(artefact))

ifeq ($(RELEASE), bullseye)
NOMERGEUSR =
PACKAGE_LIST ?= bullseye.kselftest.packages
else ifeq ($(RELEASE), bookworm)
PACKAGE_LIST ?= bookworm.kselftest.packages
NOMERGEUSR = --hook-dir=/usr/share/mmdebstrap/hooks/no-merged-usr
else ifeq ($(RELEASE), sid)
PACKAGE_LIST ?= sid.kselftest.packages
NOMERGEUSR = --hook-dir=/usr/share/mmdebstrap/hooks/no-merged-usr
endif

ifeq ($(ARCH), amd64)
TUXARCH = x86_64
TUXKERNEL = bzImage
else ifeq ($(ARCH), arm64)
TUXARCH = arm64
TUXKERNEL = Image
else ifeq ($(ARCH), armhf)
TUXARCH = armv7
TUXKERNEL = zImage
else ifeq ($(ARCH), i386)
TUXARCH = i386
TUXKERNEL = bzImage
else ifeq ($(ARCH), mipsel)
TUXARCH = mips32el
TUXKERNEL = vmlinux
else ifeq ($(ARCH), mips64el)
TUXARCH = mips64el
TUXKERNEL = vmlinux
else ifeq ($(ARCH), ppc64el)
TUXARCH = ppc64le
TUXKERNEL = vmlinux
else ifeq ($(ARCH), riscv64)
TUXARCH = riscv64
TUXKERNEL = Image
RELEASE = sid
PACKAGE_LIST = sid.kselftest.packages.riscv64
EXTRA_MIRROR = --include="debian-ports-archive-keyring" "deb http://deb.debian.org/debian-ports/ sid main" "deb http://deb.debian.org/debian-ports/ unreleased main"
else ifeq ($(ARCH), s390x)
TUXARCH = s390
TUXKERNEL = bzImage
endif

all: arch ext4

build/debian/$(RELEASE)/$(ARCH)/.dirstamp:
	@mkdir -p build/debian/$(RELEASE)/$(ARCH)
	touch $@

check: build/debian/$(RELEASE)/$(ARCH)/.dirstamp

build: check
	mmdebstrap --variant=minbase $(NOMERGEUSR) \
	   --customize-hook='chroot "$$1" passwd --delete root' \
	   --dpkgopt='path-exclude=/usr/share/man/*' \
	   --dpkgopt='path-include=/usr/share/man/man[1-9]/*' \
	   --dpkgopt='path-exclude=/usr/share/locale/*' \
	   --dpkgopt='path-include=/usr/share/locale/locale.alias' \
	   --dpkgopt='path-exclude=/usr/share/doc/*' \
	   --dpkgopt='path-include=/usr/share/doc/*/copyright' \
	   --dpkgopt='path-include=/usr/share/doc/*/changelog.Debian.*' \
	   --include="$(shell cat $(PACKAGE_LIST))" \
	   --arch=${ARCH} \
	   ${RELEASE} \
	   build/debian/$(RELEASE)/$(ARCH)/rootfs.tar \
	   $(EXTRA_MIRROR) https://deb.debian.org/debian/

compress: build
	xz -T0 build/debian/$(RELEASE)/$(ARCH)/rootfs.tar

ext4: compress arch
	fakeroot ./create-ext4.rootfs.py --rootfs build/debian/$(RELEASE)/$(ARCH)/rootfs.tar.xz --output_file build/debian/$(RELEASE)/$(ARCH)/rootfs.ext4.xz
	@rm build/debian/$(RELEASE)/$(ARCH)/rootfs.ext4.xz.tmp

arch:
	@if [ "$(ARCH)" = "" ]; then echo "ARCH is not set, values: $(ARCHS)"; exit 1; fi

test: arch
	tuxrun --device qemu-$(TUXARCH) --rootfs "build/debian/$(RELEASE)/$(ARCH)/rootfs.ext4.xz" --tests $(TUXTESTS)

$(UPLOADS): arch
	test -f build/debian/$(RELEASE)/$(ARCH)/$(subst upload-,,$@) && aws s3 cp build/debian/$(RELEASE)/$(ARCH)/$(subst upload-,,$@) s3://tuxpub-$(ENV)-storage/debian/$(RELEASE)/$(ARCH)/$(subst upload-,,$@) || true
